<?php

function dataBr($str) {
    $data = date('d/m/Y', strtotime($str));
    return $data;
}

function dataHoraBr($str) {
    $data = date('d/m/Y H:i:s', strtotime($str));
    return $data;
}

function dataDB($str) {
    $data = explode(' ', $str);
    
    if (strlen($data[0]) > 0) {
        $dataD = explode('/', $data[0]);
        $dataH = isset($data[1]) ? $data[1] : '';
        return trim($dataD[2] . '-' . $dataD[1] . '-' . $dataD[0] . ' ' . $dataH);
    }
}

function mesStringToInt($n) {
    $n = removeAcentos($n, true);
    $mes['janeiro'] = '01';
    $mes['fevereiro'] = '02';
    $mes['marco'] = '03';
    $mes['abril'] = '04';
    $mes['maio'] = '05';
    $mes['junho'] = '06';
    $mes['julho'] = '07';
    $mes['agosto'] = '08';
    $mes['setembro'] = '09';
    $mes['outubro'] = '10';
    $mes['novembro'] = '11';
    $mes['dezembro'] = '12';

    return $mes[$n];
}

function dataStringToDataBR($str) {
    $arr = explode(' de ', strtolower($str));
    return $arr[0] . '/' . mesStringToInt($arr[1]) . '/' . $arr[2];
}

function removeAcentos($string, $slug = false) {
//Setamos o localidade
    setlocale(LC_ALL, 'pt_BR');

//Verificamos se a string é UTF-8
    if (is_utf8($string))
        $string = utf8_decode($string);

//Se a flag 'slug' for verdadeira, transformamos o texto para lowercase
    if ($slug)
        $string = strtolower($string);

// Código ASCII das vogais
    $ascii['a'] = range(224, 230);
    $ascii['e'] = range(232, 235);
    $ascii['i'] = range(236, 239);
    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
    $ascii['u'] = range(249, 252);

// Código ASCII dos outros caracteres
    $ascii['b'] = array(223);
    $ascii['c'] = array(231);
    $ascii['d'] = array(208);
    $ascii['n'] = array(241);
    $ascii['y'] = array(253, 255);

//Fazemos um loop para criar as regras de troca dos caracteres acentuados
    foreach ($ascii as $key => $item) {
        $acentos = '';
        foreach ($item AS $codigo)
            $acentos .= chr($codigo);
        $troca[$key] = '/[' . $acentos . ']/i';
    }

//Aplicamos o replace com expressao regular
    $string = preg_replace(array_values($troca), array_keys($troca), $string);

// /Se a flag 'slug' for verdadeira...
    if ($slug) {
// Troca tudo que não for letra ou número por um caractere ($slug)
        $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
// Tira os caracteres ($slug) repetidos
        $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
        $string = trim($string, $slug);
    }

    return trim($string);
}

function normalizaData($data) {
    $arr = explode('/', $data);
    $ano = preg_replace('/[^0-9]/', '', $arr[2]);
    if (strlen($ano) == 2) {
        if ($ano == '00') {
            $ano = '2000';
        }elseif(substr($ano,0,1) == '0'){
            $ano = '20'.$ano;
        }
    }
    return $arr[0] . '/' . $arr[1] . '/' . $ano;
}
