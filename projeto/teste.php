<?php

chdir( 'files' );
$arquivos = glob("{*.txt}", GLOB_BRACE);

$valorAtual = $_GET["VALOR"];

$processoNumero = "";

for($i= 0; $i < count($arquivos); $i++){
    $linhas = "";
    

    if($i==$valorAtual){

        $arquivo = fopen($arquivos[$i],'r');
        while ($line = fgets($arquivo)) {
            $linhas.= checkLine($line);
        }
        echo $arquivos[$i]."<br/>";
        

        $motivo = getMotivo($linhas);

        echo("<pre>");
            print_r($motivo);
        echo("</pre>");

        echo $linhas;

        fclose($arquivo);
        exit;
    }
} 

function checkLine($linha){
    return utf8_encode($linha);
}

function getProcesso($linhas){
    $procRegex = '@PROCESSO\s*N\.°:\s*(.*?)\s*[A-Za-z]@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        return $matches[1];
    }

    $procRegex = '@PROCESSO\s*Nº\s*(.*?)\s*[A-Za-z]@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        return $matches[1];
    }

    $procRegex = '@Processo:\s*(.*?)\s*[A-Za-z]@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        return $matches[1];
    }     
}

function getMotivo($linhas){
    $result = array();
    $procsRegexRevisao = array("/revisão/", "/revisão\s+contratual/", "/cláusulas\s+contratuais/i", "/comissão\s+de\s+permanência/i", "/juros\s+moratórios/i", "/encargos/i");
    $procsRegexTarifas = array("/\bTAC\b/", "/taxa\s+de\s+abertura\s+de\s+crédito/i", "/\bTEC\b/", "/TARIFA\s+DE\s+EMISSÃO\s+DE\s+CARNÊ/i");

    $procsRegexIndenizacao = array("/indenização/i", "/dano\s+moral/i", "/negativação/i", "/reparação\s+de\s+danos/i", "/fraudulento/i", "/fraude/i", "/lucro\s+cessante/i", "/dano\s+material/i");

    foreach ($procsRegexTarifas as $regex) {
        if(preg_match($regex, $linhas, $matches)){
            array_push($result, "tarifa");
            break;
        }
    }    
    foreach ($procsRegexRevisao as $regex) {
        if(preg_match($regex, $linhas, $matches)){
            array_push($result, "acao revisional");
            break;
        }
    }
    foreach ($procsRegexIndenizacao as $regex) {
        if(preg_match($regex, $linhas, $matches)){
            array_push($result, "indenizacao");
            break;
        }
    }
    return $result;
}

function getAdvogado($linhas){

}


function getName($nome){
    $parts = explode("_", $nome);
    $nome = strtoupper($parts[0]);

    return $nome;
}

?>