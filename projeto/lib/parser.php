<?php

class Parser {

    public $_fields = array(
        'id' => '',
        'codigo' => '',
        'numero' => '',
        'uf' => '',
        'comarca' => '',
        'vara' => '',
        'nome_juiz' => '',
        'categoria_juiz' => '',
        'nome_autor' => '',
        'data_sentenca' => '',
        'advogados' => '',
        'motivo' => '',
        'produto' => '',
        'pedidos' => '',
        'resultado' => '',
        'valor_sentenca' => '',
        'argumentos_defesa' => '',
        'insumo' => '',
        'insumo_fonetico' => '',
        'motivo_acao_revisional' => '',
        'motivo_tarifas' => '',
        'motivo_indenizacao' => '',
    );

    public function __construct($fields = null) {
        if ($fields) {
            foreach ($this->_fields as $key => $value) {
                if (isset($fields[$key])) {
                    $this->_fields[$key] = $fields[$key];
                }
            }
        }
    }

    public function __call($m, $a) {
        $ver = substr($m, 0, 3);
        if ($ver == 'set' || $ver == 'get') {
            //Forma o nome do campo
            $campo = substr($m, 3, strlen($m));
            $campo = strtolower($campo{0}) . substr($campo, 1);
            $campo = strtolower(preg_replace('/([A-Z])/', '_\1', $campo));
            if ($ver == 'set') {
                $this->_fields[$campo] = $a[0];
            } else {

                return isset($this->_fields[$campo]) ? $this->_fields[$campo] : null;
            }
        }
    }

    public function getParseComarcaCidade() {
        $text = !is_utf8($this->getInsumo()) ? utf8_encode($this->getInsumo()) : '';
        $r = null;
        if (preg_match('@COMARCA\sDE\s(.*)\s\n@ix', $text, $matches)) {
            $r = (trim(substr($matches[1], 0, 20)));
        } elseif (preg_match('@\n(.*),\s\d{2}\sde\s\w.*@ix', $text, $matches)) {
            $r = (trim($matches[1]));
        }

        $this->setComarca($r);
        return $r;
    }

    public function getParseMotivo() {
        $linhas = !is_utf8($this->getInsumo()) ? utf8_encode($this->getInsumo()) : '';
        $result = array();
        $procsRegexRevisao = array("/revisão/", "/revisão\s+contratual/", "/cláusulas\s+contratuais/i", "/comissão\s+de\s+permanência/i", "/juros\s+moratórios/i", "/encargos/i");
        $procsRegexTarifas = array("/\bTAC\b/", "/taxa\s+de\s+abertura\s+de\s+crédito/i", "/\bTEC\b/", "/TARIFA\s+DE\s+EMISSÃO\s+DE\s+CARNÊ/i");

        $procsRegexIndenizacao = array("/indenização/i", "/dano\s+moral/i", "/negativação/i", "/reparação\s+de\s+danos/i", "/fraudulento/i", "/fraude/i", "/lucro\s+cessante/i", "/dano\s+material/i");

        foreach ($procsRegexTarifas as $regex) {
            if (preg_match($regex, $linhas, $matches)) {
                $this->setMotivoTarifas(1);
                array_push($result, "tarifa");
                break;
            }
        }
        foreach ($procsRegexRevisao as $regex) {
            if (preg_match($regex, $linhas, $matches)) {
                $this->setMotivoAcaoRevisional(1);
                array_push($result, "acao revisional");
                break;
            }
        }
        foreach ($procsRegexIndenizacao as $regex) {
            if (preg_match($regex, $linhas, $matches)) {
                $this->setMotivoIndenizacao(1);
                array_push($result, "indenizacao");
                break;
            }
        }
        if (count($result) > 1) {
            $this->setMotivo('MISTO');
        } else {
            $this->setMotivo(strtoupper(current($result)));
        }
        return $result;
    }

    function getParseTipoJuiz() {
        $sentenca = $this->getResultado();
        $linhas = !is_utf8($this->getInsumo()) ? utf8_encode($this->getInsumo()) : '';
        $r = null;
        if (preg_match('(JUIZ\s*DE\s*DIREITO)', strtoupper($linhas)) === 1) {
            $r = "DE DIREITO";
        }

        if (preg_match('(JUIZ\s*LEIGO\s*)', strtoupper($linhas)) === 1) {
            $r = "LEIGO";
        }

        if (preg_match('(JUIZ\s*CONCILIADOR\s*)', strtoupper($linhas)) === 1) {
            $r = "CONCILIADOR";
        }
        if ($r == null) {
            switch ($sentenca) {
                case "":
                    $r = "";
                    break;
                default:
                    $r = "DE DIREITO";
                    break;
            }
        }
        $this->setCategoriaJuiz($r);
        return $r;
    }

    public function getParseDataSentenca() {
        $text = !is_utf8($this->getInsumo()) ? utf8_encode($this->getInsumo()) : '';
        $r = null;
        $achouPadrao = false;
        if (preg_match('@((\sJULGO)\s*((IMPROC|PROC|EXTIN)\w+))@ix', $text, $matches)) {
            $text = substr($text, strpos($text, trim($matches[1])));
            $achouPadrao = true;
        } elseif (preg_match('@((\sDeclaro)\s*((IMPROC|PROC|EXTIN)\w+))@ix', $text, $matches)) {
            $text = substr($text, strpos($text, trim($matches[1])));
            $achouPadrao = true;
        } elseif (preg_match('@(sentença)@ix', $text, $matches)) {
            $text = substr($text, strpos($text, trim($matches[1])));
            $achouPadrao = true;
        } elseif (preg_match('@(julgado)@ix', $text, $matches)) {
            $text = substr($text, strpos($text, trim($matches[1])));
            $achouPadrao = true;
        }

        if ($achouPadrao) {
            if (preg_match('@\n.*,\s(\d{2}\sde\s[A-Za-zçÇ]+\sde\s\d{4})@ix', $text, $matches)) {
                $dataString = normalizaData(dataStringToDataBR(trim($matches[1])));
                $r = $dataString;
            } elseif (preg_match('@(\d{2}\/\d{2}\/\d{2,4})[\.\s\n]@ix', $text, $matches)) {
                $dataString = (trim($matches[1]));
                $r = normalizaData($dataString);
            } elseif (preg_match('@\n.*,\s(\d{2}\sde\s[A-Za-zçÇ]+\sde\s\d.\d+)[\.\s\n]@ix', $text, $matches)) {
                $dataString = normalizaData(dataStringToDataBR(trim($matches[1])));
                $r = $dataString;
            }
        }

        $this->setDataSentenca(dataDB($r));

        return $r;
    }

    public function getParseResultadoAcao() {
        $linhas = !is_utf8($this->getInsumo()) ? utf8_encode($this->getInsumo()) : '';
        $r = null;


        $motRegex = '@julgo\s*(PROC)\s*@is';
        if (preg_match($motRegex, $linhas, $matches)) {
            $r = "PROCEDENTE";
        }

        $motRegex = '@julgo\s*(IMPROC)\s*@is';
        if (preg_match($motRegex, $linhas, $matches)) {
            $r = "IMPROCEDENTE";
        }

        if (preg_match('@(JULGO\s*PROCEDENTE&JULGO\s*IMPROCEDENTE|PROCEDENTES\s*EM\s*PARTE|PARCIALMENTE\s*PROCEDENTE|PARCIALMENTE\s*PROCEDENTES)@is', $linhas)) {
            $r = "PARCIALMENTE PROCEDENTE";
        }

        if (preg_match('(P R O C E D E N T E)', $linhas)) {
            $r = "PROCEDENTE";
        }

        $motRegex = '@julgo\s*procedente\s*em\s*parte(.*?)\s*\\s@is';
        if (preg_match($motRegex, $linhas, $matches)) {
            //if(strlen ($matches[1])>5){
            $r = "PARCIALMENTE PROCEDENTE";
            //}
        }



        $motRegex = '@NÃO\s*PROCEDE\s*(.*?)\s*\\s@is';
        if (preg_match($motRegex, $linhas, $matches)) {
            $r = "IMPROCEDENTE";
        }

        if (preg_match('(JULGO IMPROCEDENTE|JULGOjulgo IMPROCEDENTE|IMPROCEDENTE|IMPROCEDENTES)', $linhas)) {
            $r = "IMPROCEDENTE";
        }



        $this->setResultado($r);
        return $r;
    }

    public function getParseProcesso() {
        $linhas = !is_utf8($this->getInsumo()) ? utf8_encode($this->getInsumo()) : '';
        $procRegex = '@PROCESSO\s*N\.°:\s*(.*?)\s*\\s@is';
        $r = null;
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@PROCESSO\s*Nº\s*(.*?)\s*\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Processo:\s*(.*?)\s*\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 10) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Proc\s*\.\s*(.*?)\s*\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Número\s*do\s*(.*?)\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Processo\s*Físico\s*nº:\s*(.*?)\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@NR\.\s*PROTOCOLO\s*:\s*(.*?)\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Processo\s*:\s*(.*?)\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@consulta\s*Descrição\s*(.*?)\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Classe\s*:\s*(.*?)\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Processo\s*nº:\s*(.*?)\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Assunto\s*:\s*(.*?)\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@Protocolo\s*nº\s*(.*?)\s*\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@informe\s*o\s*processo\s*(.*?)\s*\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }

        $procRegex = '@NU\s*:\s*(.*?)\s*\\s@is';
        if (preg_match($procRegex, $linhas, $matches)) {
            if (strlen($matches[1]) > 5) {
                if (is_numeric($matches[1][0])) {
                    $r = $matches[1];
                }
            }
        }
        $this->setNumero($r);
        return $r;
    }

}
