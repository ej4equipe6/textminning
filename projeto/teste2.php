<?php

$arr = $_GET['arr'];
chdir( 'files' );
$arquivos = glob("{*.txt}", GLOB_BRACE);

$processoNumero = $resultadoAcao = $tipoJuiz = $nomeJuiz ="";

for($i= 0; $i < count($arquivos); $i++){
    $linhas = "";
    

    if($i==$arr){

        $arquivo = fopen($arquivos[$i],'r');
        while ($line = fgets($arquivo)) {
            $linhas.= checkLine($line);
        }

        $processoNumero = getProcesso($linhas);
        $resultadoAcao = getResultadoAcao($linhas);
        $tipoJuiz = getTipoJuiz($linhas,$resultadoAcao);




        echo $processoNumero."<br/>";
        echo $resultadoAcao. "<br/>";
        echo $arquivos[$i]."<br/>";
        echo $tipoJuiz."<br/>";
        echo $nomeJuiz."<br/>";
        echo $linhas."<br/><br/>";

        

        fclose($arquivo);
        exit;
    }


  
} 

function checkLine($linha){
    return utf8_encode($linha);
}

function getProcesso($linhas){
    $procRegex = '@PROCESSO\s*N\.°:\s*(.*?)\s*\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen ($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }  
    }

    $procRegex = '@PROCESSO\s*Nº\s*(.*?)\s*\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }
    
    $procRegex = '@Processo:\s*(.*?)\s*\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>10){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@Proc\s*\.\s*(.*?)\s*\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@Número\s*do\s*(.*?)\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@Processo\s*Físico\s*nº:\s*(.*?)\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@NR\.\s*PROTOCOLO\s*:\s*(.*?)\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@Processo\s*:\s*(.*?)\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@consulta\s*Descrição\s*(.*?)\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@Classe\s*:\s*(.*?)\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@Processo\s*nº:\s*(.*?)\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@Assunto\s*:\s*(.*?)\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }  

    $procRegex = '@Protocolo\s*nº\s*(.*?)\s*\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen ($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }
    
    $procRegex = '@informe\s*o\s*processo\s*(.*?)\s*\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen ($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }

    $procRegex = '@NU\s*:\s*(.*?)\s*\\s@is';
    if (preg_match($procRegex, $linhas, $matches)) {
        if(strlen ($matches[1])>5){
            if(is_numeric($matches[1][0])){
                return $matches[1];
            }
        }
    }
    return null;
}





function getResultadoAcao($linhas){

    if(preg_match('(JULGO\s*PROCEDENTE&JULGO\s*IMPROCEDENTE|PROCEDENTES\s*EM\s*PARTE|PARCIALMENTE\s*PROCEDENTE|PARCIALMENTE\s*PROCEDENTES)', strtoupper($linhas)) === 1) {
        return "PARCIALMENTE PROCEDENTE"; 
    } 

    if(preg_match('(P R O C E D E N T E)', strtoupper($linhas)) === 1) {
        return "PROCEDENTE"; 
    } 

    

    $motRegex = '@julgo\s*procedente\s*em\s*parte(.*?)\s*\\s@is';
    if (preg_match($motRegex, $linhas, $matches)) {
        //if(strlen ($matches[1])>5){
            return "PARCIALMENTE PROCEDENTE";
        //}
    }

    $motRegex = '@julgo\s*(.*?)\s*\\s@is';
    if (preg_match($motRegex, $linhas, $matches)) {
        if(strlen ($matches[1])>5){
            return strtoupper($matches[1]);
        }
    }

    $motRegex = '@NÃO\s*PROCEDE\s*(.*?)\s*\\s@is';
    if (preg_match($motRegex, $linhas, $matches)) {
        return "IMPROCEDENTE";
        
    }

    if(preg_match('(JULGO IMPROCEDENTE|JULGOjulgo IMPROCEDENTE|IMPROCEDENTE|IMPROCEDENTES)', strtoupper($linhas)) === 1) {
        return "IMPROCEDENTE"; 
    } 
}


function getTipoJuiz($linhas, $sentenca){
    if(preg_match('(JUIZ\s*DE\s*DIREITO)', strtoupper($linhas))=== 1) {
        return "DE DIREITO"; 
    }
    
    if(preg_match('(JUIZ\s*LEIGO\s*)', strtoupper($linhas))=== 1) {
        return "LEIGO"; 
    }

    if(preg_match('(JUIZ\s*CONCILIADOR\s*)', strtoupper($linhas))=== 1) {
        return "CONCILIADOR"; 
    }

    switch($sentenca){
        case "":
            return "";
            break;
        default:
            return "DE DIREITO";
            break;
    }
}





function getName($nome){
    $parts = explode("_", $nome);
    $nome = strtoupper($parts[0]);

    return $nome;
}

function get_word_counts($phrases) {
    $counts = array();
     foreach ($phrases as $phrase) {
         $words = explode(' ', $phrase);
         foreach ($words as $word) {
           $word = preg_replace("#[^a-zA-Z\-]#", "", $word);
             $counts[$word] += 1;
         }
     }
     return $counts;
 }

?>